package by.muhachiov.phonebook.controllers;

import by.muhachiov.phonebook.Person;
import by.muhachiov.phonebook.Phone;
import by.muhachiov.phonebook.PhonebookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/")
public class PersonsControler {

    @Autowired
    private PhonebookService service;

    @RequestMapping(value = "/create", method = RequestMethod.GET)
    public String create(ModelMap model) {
        Person person = new Person();
        model.addAttribute("person", person);
        return "create";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public ModelAndView savePerson(@ModelAttribute("command") Person person, @RequestParam(value = "person_phone", required = false) String phone_number) {

        List<Phone> phones = new ArrayList<Phone>();
        Phone phone = new Phone();
        phone.setPerson(person);
        phone.setPhone_number(phone_number);
        phones.add(phone);
        person.setPhones(phones);
        service.create(person);
        return new ModelAndView("redirect:/all");
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String update(ModelMap model, @RequestParam(value = "id", required = true) Integer id) {
        Person person = service.getById(id);
        model.addAttribute("person", person);
        return "update";
    }

    @RequestMapping(value = "/edit", method = RequestMethod.POST)
    public ModelAndView editPerson(@ModelAttribute("command") Person person, ModelMap model, @RequestParam
            (value = "person_id", required = true) Integer person_id, @RequestParam(value = "person_phone", required = false) String[] phone_numbers) {

        service.delete(person_id);

        List<Phone> phones = new ArrayList<Phone>();
        for (String phone2 : phone_numbers) {
            Phone phone = new Phone();
            phone.setPerson(person);
            phone.setPhone_number(phone2);
            phones.add(phone);
        }
        person.setPhones(phones);
        service.create(person);

        return new ModelAndView("redirect:/all");
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    public String printAll(ModelMap modelAll) {
        modelAll.addAttribute("person", service.getAll());
        return "all";
    }

    @RequestMapping(value = "/delete", method = RequestMethod.GET)
    public ModelAndView delete(ModelMap model, @RequestParam(value = "id", required = true) Integer id) {
        service.delete(id);
        return new ModelAndView("redirect:/all");
    }
}