package by.muhachiov.phonebook;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Transactional
@Repository("HibernateUtil")
public class HibernateUtil {

    //private static SessionFactory sessionFactory = null;
//    public static SessionFactory sessionFactory = null;

    //    static {
//        try {
//            sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
//        } catch (Exception e) {
//            System.out.println(e);
//        }
//    }
    private static SessionFactory sessionFactory;


    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    @Resource(name = "sessionFactory")
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


}


//        import com.springapp.mvc.domain.Users;
//        import org.hibernate.Query;
//        import org.hibernate.Session;
//        import org.hibernate.SessionFactory;
//        import org.springframework.stereotype.Repository;
//        import org.springframework.transaction.annotation.Transactional;
//
//        import javax.annotation.Resource;
//
//@Transactional
//@Repository("LoginDao")
//public class LoginDao {
//
//
//    public SessionFactory getMy_sessionfactory() {
//        return my_sessionfactory;
//    }
//
//    @Resource(name = "sessionFactory")
//    public void setMy_sessionfactory(SessionFactory my_sessionfactory) {
//        this.my_sessionfactory = my_sessionfactory;
//    }
//
//    private SessionFactory my_sessionfactory;
//
//    public Long findByUname(Users loginForm) {
//        System.out.println("Hi Dao" + loginForm.getUname());
//        Session session = my_sessionfactory.getCurrentSession();
//        String hql = "select count(u.uname) from Users u where u.uname = ? and u.pass = ?";
//        Query query = session.createQuery(hql).setString(0, loginForm.getUname()).setString(1, loginForm.getPass());
//        System.out.println("query" + query);
//        System.out.println("query ur" + query.uniqueResult());
//
//        Long count = (Long) query.uniqueResult();
//        return count;
//    }
//
//
//}