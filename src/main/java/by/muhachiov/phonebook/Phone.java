package by.muhachiov.phonebook;

import javax.persistence.*;

@Entity
@Table(name = "phone")
public class Phone {

    @ManyToOne
    @JoinColumn(name = "person_id", referencedColumnName = "person_id")
    private Person person;

    @Id
    @Column(name = "phone_id", unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pkp_sequence")
    @SequenceGenerator(name = "pkp_sequence", sequenceName = "phone_phone_id_seq", allocationSize = 1)

    private Integer phone_id;

    @Column(name = "phone_number")
    private String phone_number;

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public Integer getPhone_id() {
        return phone_id;
    }

    public void setPhone_id(Integer phone_id) {
        this.phone_id = phone_id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String toString() {
        return "person = " + person + "phone_id = " + phone_id + "phone_number = " + phone_number;
    }
}