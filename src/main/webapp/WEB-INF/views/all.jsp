<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<%@ page language="java" contentType="text/html; charset=UTF-8"  pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>all</title>
</head>

<body>

<table border="3px">

    <thead>
    <th>Id</th>
    <th>Surname</th>
    <th>Name</th>
    <th>Patronymic</th>
    <th>Address</th>
    <th>Phone</th>
    </thead>

    <tbody>
    <c:forEach var="cur_person" items="${person}">
        <tr>
            <td><c:out value="${cur_person.person_id}"/></td>
            <td>
                <c:out value="${cur_person.surname}"></c:out>
            </td>
            <td>
                <c:out value="${cur_person.name}"></c:out>
            </td>
            <td>
                <c:out value="${cur_person.patronymic}"></c:out>
            </td>
            <td>
                <c:out value="${cur_person.address}"></c:out>
            </td>
            <td>
                <table>
                    <c:forEach var="person_phone" items="${cur_person.phones}">
                        <tr>
                            <td>
                                <c:out value="${person_phone.phone_number}"></c:out>
                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </td>
            <td>

                <a href="http://localhost:8080/person/update?id=${cur_person.person_id}">update</a><br>
                <a href="http://localhost:8080/person/delete?id=${cur_person.person_id}">delete</a><br>

            </td>
        </tr>
    </c:forEach>
    </tbody>

</table>

<a href="http://localhost:8080/person/create">create</a><br>

</body>

</html>