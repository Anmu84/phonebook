<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>all</title>
</head>

<body>

<h1>Create person</h1>

<p>Here you can add a new person.</p>

<form:form method="POST" action="/person/save">
<table>
    <tbody>
    <tr>
        <td>Surname:</td>
        <td><input type="text" name="surname" value="${person.surname}"/></td>
    </tr>
    <tr>
        <td>Name:</td>
        <td><input type="text" name="name" value="${person.name}"/></td>
    </tr>
    <tr>
        <td>Patronymic:</td>
        <td><input type="text" name="patronymic" value="${person.patronymic}"/></td>
    </tr>
    <tr>
        <td>Address:</td>
        <td><input type="text" name="address" value="${person.address}"/></td>
    </tr>
    <tr1>
        <td>Phone:</td>
        <p1>
            <td><input type="text" name="person_phone" value="${person.phones}"/></td>
        </p1>
    </tr1>
    <tr>
        <td><input type="submit" value="save"></td>
        <td></td>
    </tr>
    </tbody>
</table>
</form:form>

<p><a href="http://localhost:8080/person/all"><h3>all</h3></a>
<p>